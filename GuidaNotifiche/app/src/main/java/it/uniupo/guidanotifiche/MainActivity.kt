package it.uniupo.guidanotifiche

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseMessaging.getInstance().token.addOnSuccessListener { token ->
            if(token != null){
                //in questo esempio il token viene stampato e inserito manualmente nel codice del server
                Log.d("guidanotifichetoken", token)
            }
        }

    }
}