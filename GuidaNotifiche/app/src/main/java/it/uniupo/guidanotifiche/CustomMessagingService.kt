package it.uniupo.guidanotifiche

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class CustomMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        val text = p0.data["text"]
        if(text != null){
            val builder = NotificationBuilder(this)
            builder.create("Title", text)
            builder.send()
        }
    }

}