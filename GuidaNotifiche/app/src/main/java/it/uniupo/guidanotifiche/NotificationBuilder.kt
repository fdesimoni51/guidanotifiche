package it.uniupo.guidanotifiche

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class NotificationBuilder(cont: Context) {

    var notification : Notification? = null
    val context : Context
    val notChanId = "GUIDANOTIFICHE"

    init {
        context = cont
        makeChannel()
    }

    fun create(title: String, text: String){

        notification = NotificationCompat.Builder(context, notChanId)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setAutoCancel(true)
            .build()
    }

    private fun makeChannel(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            val name = "Name"
            val descriptionText = "Description"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(notChanId, name, importance).apply {
                description = descriptionText
            }
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun send(){
        if(notification != null){
            val id = System.currentTimeMillis().toInt()
            NotificationManagerCompat.from(context).notify(id, notification!!)
        }
    }

}