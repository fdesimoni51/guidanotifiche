const serviceAccount = require("path/to/private/key")
const admin = require("firebase-admin")

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });

var message = {data: {text:"text inside data message", field1:"field1", field2:"field2"}, token: "di-gU_zCTHG5B..."};
admin.messaging().send(message).then((response) => {
    console.log('message sent', response);
    return null
}).catch((error) => {
    console.log('error while sending message', error);
});
